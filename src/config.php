<?php

return [
    // 默认磁盘
    'default' => 'public',
    'disks'   => [
        //本地存储
        'public' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/upload',
            // 可见性
            'visibility' => 'public',
        ],
        //七牛存储
        "qiniu" => [
            "type" => "qiniu",
            "accessKey" => "",
            "secretKey" => "",
            "bucket" => "",
            "domain" => "https://up-z1.qiniup.com",
            "cdn" => ""
        ],
        //阿里云存储
        "oss" => [
            "type" => "oss",
            "accessId" => "",
            "accessSecret" => "",
            "bucket" => "",
            "endpoint" => "oss-cn-beijing.aliyuncs.com",
            "domain" => "",
            "cdn" => ""
        ],
        //腾讯云存储
        "cos"    => [
            "type"        => "cos",
            "region"      => "",
            "credentials" => [
                "appId"     => "",
                "secretId"  => "",
                "secretKey" => ""
            ],
            "bucket"      => "",
            "domain"      => "",
            "scheme"      => "https",
            "cdn"         => "",
        ]
    ],
];